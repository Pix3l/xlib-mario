# Xlib Mario

This is a simple and basic **Super Mario Bros.** DOS clone by **Billy Buerger**, 1996.  

There's a **SMB.EXE** DOS executable file that can be runned in **Dosbox**.  
The game is pretty bare, there's no enemies or power ups, nothing more than a technical demo of a platform game.

I uploaded this source code only for historycal reasons, as how it was.  

The original site was accessible at [http://www.msoe.edu/~buergerw/](http://www.msoe.edu/~buergerw/progs/), and the project description was as follows:

```
Super Mario Bros.
Being a computer engineer major, I like to program stuff. My biggest project being Super Mario Bros. I started work on it sometime around last winter. I am constantly updating it and adding new things.
Download the latest version of my Super Mario Bros. game!
SMB.ZIP as of December 10, 1996

New features
Increased the speed. It even runs decent on my 386.
Coins flip up when you hit a block.
Hidden blocks.
```

The source is released as public domain.